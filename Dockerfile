FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

ARG VERSION=1.10.6

RUN mkdir -p /app/code
WORKDIR /app/code

RUN curl -L https://github.com/schlagmichdoch/PairDrop/archive/refs/tags/v${VERSION}.zip -o /tmp/tmp.zip && unzip /tmp/tmp.zip -d /tmp && rm /tmp/tmp.zip
RUN mv /tmp/PairDrop-${VERSION}/* /app/code/

RUN npm ci

# environment settings
ENV NODE_ENV="production"

EXPOSE 3000

HEALTHCHECK --interval=30s --timeout=10s --start-period=5s --retries=3 \
  CMD wget --quiet --tries=1 --spider http://localhost:3000 || exit 1

ENTRYPOINT ["npm", "start"]
