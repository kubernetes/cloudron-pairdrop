#!/bin/bash

# Set HOME-Folder to /app/data because /root is read-only
export HOME=/app/data

setup() {
  # Install cloudron cli with npm and use /app/data/cache for caching because normal cache path is not writable
  npm install --cache /app/data/cache/ cloudron --prefix /app/data/
}

install_app() {
  # Read config.yml
  while IFS='=' read -r key value; do
    if [[ $key && $value ]]; then
      export "$key"="$value"
    fi
  done < config.ini

  # install custom app to cloudron
  /app/data/node_modules/cloudron/bin/cloudron --token "$ACCESS_TOKEN" --server "$CLOUDRON_SERVER" install --image "$CUSTOM_APP_IMAGE" --no-wait -l "$LOCATION_OF_INSTALLED_APP"
}

setup
install_app
